# Use the base OpenJDK 17 image
FROM openjdk:17-jdk-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the JAR file to the container
COPY target/*.jar .

# Specify the command to run the application
CMD ["java", "-jar", "*.jar"]
